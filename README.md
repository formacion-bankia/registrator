# README #

Proyecto de ejemplo de utilización de Registrator + Consul

Importante: debe ejecutarse sobre máquina Linux para poder bindear el sock de Docker del host.


### How do I get set up? ###

* Instanciación de un agente de Consul:

```
docker run -d -p 8301:8301 -p 8302:8302 -p 8500:8500 -p 8600:8600 --name=dev-consul consul

```

* Instanciación de registrator:

```
docker run -d --name=registrator --net=host --volume=/var/run/docker.sock:/tmp/docker.sock    gliderlabs/registrator:latest consul://localhost:8500

```


* Despliegue de varias instancias de aplicación ejemplo:

```
docker run -d --name hello -p 5000:5000 mulspace/hello
docker run -d --name hello2 -p 5001:5000 mulspace/hello
docker run -d --name hello3 -p 5002:5000 mulspace/hello

```

* Verificación en http://localhost:8500